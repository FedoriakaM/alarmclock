﻿namespace AlarmClock
{
    partial class Application
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Application));
            this.Time = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Label();
            this.MinimizeButton = new System.Windows.Forms.Label();
            this.ModeSwitcher = new ColorSlider.ColorSlider();
            this.OnLabel = new System.Windows.Forms.Label();
            this.OffLabel = new System.Windows.Forms.Label();
            this.RadioLabel = new System.Windows.Forms.Label();
            this.TimerLabel = new System.Windows.Forms.Label();
            this.ModeSWLabel = new System.Windows.Forms.Label();
            this.AlarmTimeLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.SetupTimeBtn = new System.Windows.Forms.Button();
            this.SetupAlarmBtn = new System.Windows.Forms.Button();
            this.MinsBtn = new System.Windows.Forms.Button();
            this.HoursBtn = new System.Windows.Forms.Button();
            this.StationsList = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.VolumeSlider = new ColorSlider.ColorSlider();
            this.TagLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.RadioSTButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Time
            // 
            this.Time.AutoSize = true;
            this.Time.Font = new System.Drawing.Font("Arial", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Time.ForeColor = System.Drawing.SystemColors.Window;
            this.Time.Location = new System.Drawing.Point(12, 49);
            this.Time.Name = "Time";
            this.Time.Size = new System.Drawing.Size(290, 75);
            this.Time.TabIndex = 0;
            this.Time.Text = "00:00:00";
            // 
            // CloseButton
            // 
            this.CloseButton.AutoSize = true;
            this.CloseButton.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CloseButton.ForeColor = System.Drawing.SystemColors.Window;
            this.CloseButton.Location = new System.Drawing.Point(537, 3);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(22, 24);
            this.CloseButton.TabIndex = 1;
            this.CloseButton.Text = "x";
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            this.CloseButton.MouseLeave += new System.EventHandler(this.CloseButton_MouseLeave);
            this.CloseButton.MouseHover += new System.EventHandler(this.CloseButton_MouseHover);
            // 
            // MinimizeButton
            // 
            this.MinimizeButton.AutoSize = true;
            this.MinimizeButton.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinimizeButton.ForeColor = System.Drawing.SystemColors.Window;
            this.MinimizeButton.Location = new System.Drawing.Point(510, 3);
            this.MinimizeButton.Name = "MinimizeButton";
            this.MinimizeButton.Size = new System.Drawing.Size(22, 24);
            this.MinimizeButton.TabIndex = 2;
            this.MinimizeButton.Text = "_";
            this.MinimizeButton.Click += new System.EventHandler(this.MinimizeButton_Click);
            this.MinimizeButton.MouseLeave += new System.EventHandler(this.MinimizeButton_MouseLeave);
            this.MinimizeButton.MouseHover += new System.EventHandler(this.MinimizeButton_MouseHover);
            // 
            // ModeSwitcher
            // 
            this.ModeSwitcher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ModeSwitcher.BarPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(94)))), ((int)(((byte)(110)))));
            this.ModeSwitcher.BarPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(60)))), ((int)(((byte)(74)))));
            this.ModeSwitcher.BorderRoundRectSize = new System.Drawing.Size(8, 8);
            this.ModeSwitcher.ColorSchema = ColorSlider.ColorSlider.ColorSchemas.RedColors;
            this.ModeSwitcher.ElapsedInnerColor = System.Drawing.Color.Black;
            this.ModeSwitcher.ElapsedPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(94)))), ((int)(((byte)(110)))));
            this.ModeSwitcher.ElapsedPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(60)))), ((int)(((byte)(74)))));
            this.ModeSwitcher.LargeChange = ((uint)(5u));
            this.ModeSwitcher.Location = new System.Drawing.Point(323, 72);
            this.ModeSwitcher.Maximum = 4;
            this.ModeSwitcher.Minimum = 1;
            this.ModeSwitcher.Name = "ModeSwitcher";
            this.ModeSwitcher.ScaleDivisions = 3;
            this.ModeSwitcher.ScaleSubDivisions = 5;
            this.ModeSwitcher.ShowDivisionsText = false;
            this.ModeSwitcher.ShowSmallScale = false;
            this.ModeSwitcher.Size = new System.Drawing.Size(200, 48);
            this.ModeSwitcher.SmallChange = ((uint)(1u));
            this.ModeSwitcher.TabIndex = 4;
            this.ModeSwitcher.Text = "ModeSwitch";
            this.ModeSwitcher.ThumbInnerColor = System.Drawing.Color.Red;
            this.ModeSwitcher.ThumbPenColor = System.Drawing.Color.Red;
            this.ModeSwitcher.ThumbRoundRectSize = new System.Drawing.Size(16, 16);
            this.ModeSwitcher.ThumbSize = new System.Drawing.Size(16, 16);
            this.ModeSwitcher.TickColor = System.Drawing.Color.White;
            this.ModeSwitcher.TickStyle = System.Windows.Forms.TickStyle.BottomRight;
            this.ModeSwitcher.Value = 2;
            this.ModeSwitcher.ValueChanged += new System.EventHandler(this.ModeSwitcher_ValueChanged);
            // 
            // OnLabel
            // 
            this.OnLabel.AutoSize = true;
            this.OnLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OnLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.OnLabel.Location = new System.Drawing.Point(319, 123);
            this.OnLabel.Name = "OnLabel";
            this.OnLabel.Size = new System.Drawing.Size(27, 20);
            this.OnLabel.TabIndex = 5;
            this.OnLabel.Text = "On";
            // 
            // OffLabel
            // 
            this.OffLabel.AutoSize = true;
            this.OffLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OffLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.OffLabel.Location = new System.Drawing.Point(380, 123);
            this.OffLabel.Name = "OffLabel";
            this.OffLabel.Size = new System.Drawing.Size(27, 20);
            this.OffLabel.TabIndex = 6;
            this.OffLabel.Text = "Off";
            // 
            // RadioLabel
            // 
            this.RadioLabel.AutoSize = true;
            this.RadioLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RadioLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.RadioLabel.Location = new System.Drawing.Point(432, 123);
            this.RadioLabel.Name = "RadioLabel";
            this.RadioLabel.Size = new System.Drawing.Size(45, 20);
            this.RadioLabel.TabIndex = 7;
            this.RadioLabel.Text = "Radio";
            // 
            // TimerLabel
            // 
            this.TimerLabel.AutoSize = true;
            this.TimerLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TimerLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.TimerLabel.Location = new System.Drawing.Point(493, 123);
            this.TimerLabel.Name = "TimerLabel";
            this.TimerLabel.Size = new System.Drawing.Size(45, 20);
            this.TimerLabel.TabIndex = 8;
            this.TimerLabel.Text = "Timer";
            // 
            // ModeSWLabel
            // 
            this.ModeSWLabel.AutoSize = true;
            this.ModeSWLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ModeSWLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.ModeSWLabel.Location = new System.Drawing.Point(380, 49);
            this.ModeSWLabel.Name = "ModeSWLabel";
            this.ModeSWLabel.Size = new System.Drawing.Size(88, 20);
            this.ModeSWLabel.TabIndex = 9;
            this.ModeSWLabel.Text = "Mode Switch";
            // 
            // AlarmTimeLabel
            // 
            this.AlarmTimeLabel.AutoSize = true;
            this.AlarmTimeLabel.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AlarmTimeLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.AlarmTimeLabel.Location = new System.Drawing.Point(125, 127);
            this.AlarmTimeLabel.Name = "AlarmTimeLabel";
            this.AlarmTimeLabel.Size = new System.Drawing.Size(61, 22);
            this.AlarmTimeLabel.TabIndex = 10;
            this.AlarmTimeLabel.Text = "00:00";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(99, 126);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(20, 22);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // SetupTimeBtn
            // 
            this.SetupTimeBtn.BackColor = System.Drawing.SystemColors.Window;
            this.SetupTimeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SetupTimeBtn.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SetupTimeBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.SetupTimeBtn.Location = new System.Drawing.Point(448, 163);
            this.SetupTimeBtn.Name = "SetupTimeBtn";
            this.SetupTimeBtn.Size = new System.Drawing.Size(90, 23);
            this.SetupTimeBtn.TabIndex = 12;
            this.SetupTimeBtn.Text = "Setup Time";
            this.SetupTimeBtn.UseVisualStyleBackColor = false;
            this.SetupTimeBtn.Click += new System.EventHandler(this.SetupTimeBtn_Click);
            // 
            // SetupAlarmBtn
            // 
            this.SetupAlarmBtn.BackColor = System.Drawing.SystemColors.Window;
            this.SetupAlarmBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SetupAlarmBtn.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SetupAlarmBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.SetupAlarmBtn.Location = new System.Drawing.Point(448, 192);
            this.SetupAlarmBtn.Name = "SetupAlarmBtn";
            this.SetupAlarmBtn.Size = new System.Drawing.Size(90, 23);
            this.SetupAlarmBtn.TabIndex = 13;
            this.SetupAlarmBtn.Text = "Setup Alarm";
            this.SetupAlarmBtn.UseVisualStyleBackColor = false;
            this.SetupAlarmBtn.Click += new System.EventHandler(this.SetupAlarmBtn_Click);
            // 
            // MinsBtn
            // 
            this.MinsBtn.BackColor = System.Drawing.SystemColors.Window;
            this.MinsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinsBtn.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinsBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.MinsBtn.Location = new System.Drawing.Point(367, 163);
            this.MinsBtn.Name = "MinsBtn";
            this.MinsBtn.Size = new System.Drawing.Size(64, 52);
            this.MinsBtn.TabIndex = 14;
            this.MinsBtn.Text = "Minutes";
            this.MinsBtn.UseVisualStyleBackColor = false;
            this.MinsBtn.Click += new System.EventHandler(this.MinsBtn_Click);
            // 
            // HoursBtn
            // 
            this.HoursBtn.BackColor = System.Drawing.SystemColors.Window;
            this.HoursBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HoursBtn.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HoursBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.HoursBtn.Location = new System.Drawing.Point(282, 163);
            this.HoursBtn.Name = "HoursBtn";
            this.HoursBtn.Size = new System.Drawing.Size(64, 52);
            this.HoursBtn.TabIndex = 15;
            this.HoursBtn.Text = "Hours";
            this.HoursBtn.UseVisualStyleBackColor = false;
            this.HoursBtn.Click += new System.EventHandler(this.HoursBtn_Click);
            // 
            // StationsList
            // 
            this.StationsList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.StationsList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.StationsList.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StationsList.ForeColor = System.Drawing.SystemColors.Window;
            this.StationsList.FormattingEnabled = true;
            this.StationsList.ItemHeight = 18;
            this.StationsList.Location = new System.Drawing.Point(44, 282);
            this.StationsList.Name = "StationsList";
            this.StationsList.Size = new System.Drawing.Size(144, 162);
            this.StationsList.TabIndex = 16;
            this.StationsList.SelectedIndexChanged += new System.EventHandler(this.StationsList_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.Window;
            this.label1.Location = new System.Drawing.Point(22, 250);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 18);
            this.label1.TabIndex = 17;
            this.label1.Text = "Radio Stations:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.Window;
            this.label2.Location = new System.Drawing.Point(204, 380);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 18);
            this.label2.TabIndex = 18;
            this.label2.Text = "Volume";
            // 
            // VolumeSlider
            // 
            this.VolumeSlider.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.VolumeSlider.BarPenColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(94)))), ((int)(((byte)(110)))));
            this.VolumeSlider.BarPenColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(60)))), ((int)(((byte)(74)))));
            this.VolumeSlider.BorderRoundRectSize = new System.Drawing.Size(8, 8);
            this.VolumeSlider.ColorSchema = ColorSlider.ColorSlider.ColorSchemas.RedColors;
            this.VolumeSlider.ElapsedInnerColor = System.Drawing.Color.Red;
            this.VolumeSlider.ElapsedPenColorBottom = System.Drawing.Color.Salmon;
            this.VolumeSlider.ElapsedPenColorTop = System.Drawing.Color.LightCoral;
            this.VolumeSlider.LargeChange = ((uint)(5u));
            this.VolumeSlider.Location = new System.Drawing.Point(220, 401);
            this.VolumeSlider.Name = "VolumeSlider";
            this.VolumeSlider.ScaleDivisions = 3;
            this.VolumeSlider.ScaleSubDivisions = 5;
            this.VolumeSlider.ShowDivisionsText = false;
            this.VolumeSlider.ShowSmallScale = false;
            this.VolumeSlider.Size = new System.Drawing.Size(118, 48);
            this.VolumeSlider.SmallChange = ((uint)(1u));
            this.VolumeSlider.TabIndex = 19;
            this.VolumeSlider.Text = "ModeSwitch";
            this.VolumeSlider.ThumbInnerColor = System.Drawing.Color.Red;
            this.VolumeSlider.ThumbPenColor = System.Drawing.Color.Red;
            this.VolumeSlider.ThumbRoundRectSize = new System.Drawing.Size(16, 16);
            this.VolumeSlider.ThumbSize = new System.Drawing.Size(16, 16);
            this.VolumeSlider.TickColor = System.Drawing.Color.White;
            this.VolumeSlider.TickStyle = System.Windows.Forms.TickStyle.None;
            this.VolumeSlider.Value = 50;
            this.VolumeSlider.ValueChanged += new System.EventHandler(this.VolumeSlider_ValueChanged);
            // 
            // TagLabel
            // 
            this.TagLabel.AutoSize = true;
            this.TagLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TagLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.TagLabel.Location = new System.Drawing.Point(217, 285);
            this.TagLabel.Name = "TagLabel";
            this.TagLabel.Size = new System.Drawing.Size(76, 15);
            this.TagLabel.TabIndex = 20;
            this.TagLabel.Text = "Now Playing";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.Window;
            this.label4.Location = new System.Drawing.Point(204, 250);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 18);
            this.label4.TabIndex = 21;
            this.label4.Text = "Now Playing:";
            // 
            // RadioSTButton
            // 
            this.RadioSTButton.BackColor = System.Drawing.SystemColors.Window;
            this.RadioSTButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RadioSTButton.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RadioSTButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.RadioSTButton.Location = new System.Drawing.Point(448, 221);
            this.RadioSTButton.Name = "RadioSTButton";
            this.RadioSTButton.Size = new System.Drawing.Size(90, 23);
            this.RadioSTButton.TabIndex = 22;
            this.RadioSTButton.Text = "Setup Radio";
            this.RadioSTButton.UseVisualStyleBackColor = false;
            this.RadioSTButton.Click += new System.EventHandler(this.RadioSTButton_Click);
            // 
            // Application
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(562, 471);
            this.Controls.Add(this.RadioSTButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TagLabel);
            this.Controls.Add(this.VolumeSlider);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.StationsList);
            this.Controls.Add(this.HoursBtn);
            this.Controls.Add(this.MinsBtn);
            this.Controls.Add(this.SetupAlarmBtn);
            this.Controls.Add(this.SetupTimeBtn);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.AlarmTimeLabel);
            this.Controls.Add(this.ModeSWLabel);
            this.Controls.Add(this.TimerLabel);
            this.Controls.Add(this.RadioLabel);
            this.Controls.Add(this.OffLabel);
            this.Controls.Add(this.OnLabel);
            this.Controls.Add(this.ModeSwitcher);
            this.Controls.Add(this.MinimizeButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.Time);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Application";
            this.Text = "Application";
            this.Load += new System.EventHandler(this.Application_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Application_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Application_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Application_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Time;
        private System.Windows.Forms.Label CloseButton;
        private System.Windows.Forms.Label MinimizeButton;
        internal ColorSlider.ColorSlider ModeSwitcher;
        private System.Windows.Forms.Label OnLabel;
        private System.Windows.Forms.Label OffLabel;
        private System.Windows.Forms.Label RadioLabel;
        private System.Windows.Forms.Label TimerLabel;
        private System.Windows.Forms.Label ModeSWLabel;
        private System.Windows.Forms.Label AlarmTimeLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button SetupTimeBtn;
        private System.Windows.Forms.Button SetupAlarmBtn;
        private System.Windows.Forms.Button MinsBtn;
        private System.Windows.Forms.Button HoursBtn;
        internal System.Windows.Forms.ListBox StationsList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private ColorSlider.ColorSlider VolumeSlider;
        private System.Windows.Forms.Label TagLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button RadioSTButton;
    }
}