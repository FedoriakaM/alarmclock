﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmClock
{
    class Clock
    {
        public DateTime CurrentTime { get; set; } //Властивість, що зберігає поточний час
        private Timer timer; //Внутрішній таймер годинника
        public Clock() //При ініціалізації об'єкту береться поточний час з комп'ютера користувача і запускається таймер, що рахує час посекундно
        {
            CurrentTime = DateTime.Now;
            timer = new Timer();
            timer.Interval = 1000;
            timer.Tick += delegate (object sender, EventArgs e)
              {
                  CurrentTime = CurrentTime.AddSeconds(1);
              };
            timer.Start();
        }
        public string GetTime() //Представлення поточного часу у необхідному вигляді
        {
            return (CurrentTime.Hour < 10 ? "0" + CurrentTime.Hour.ToString() : CurrentTime.Hour.ToString()) + ":" +
                (CurrentTime.Minute < 10 ? "0" + CurrentTime.Minute.ToString() : CurrentTime.Minute.ToString()) + ":" +
                (CurrentTime.Second < 10 ? "0" + CurrentTime.Second.ToString() : CurrentTime.Second.ToString());
        }
    }
}
