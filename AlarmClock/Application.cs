﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmClock
{
    public partial class Application : Form
    {
        private bool drag;
        private int mouseX;
        private int mouseY;
        internal AlarmClock ThisClock;
        private int ButtonMode;
        internal Radio MyRadio;
        Timer tt;

        public Application()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }

        private void Application_MouseDown(object sender, MouseEventArgs e)
        {
            drag = true;
            mouseX = Cursor.Position.X - this.Left;
            mouseY = Cursor.Position.Y - this.Top;
        }

        private void Application_MouseUp(object sender, MouseEventArgs e)
        {
            drag = false;
        }

        private void Application_MouseMove(object sender, MouseEventArgs e)
        {
            if (drag)
            {
                this.Left = Cursor.Position.X - mouseX;
                this.Top = Cursor.Position.Y - mouseY;
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void MinimizeButton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Application_Load(object sender, EventArgs e)
        {
            StationsList.DrawMode = DrawMode.OwnerDrawFixed;
            StationsList.DrawItem += new DrawItemEventHandler(StationsList_DrawItem);

            StationsList.Enabled = false;
            VolumeSlider.Enabled = false;
            HoursBtn.Visible = false;
            MinsBtn.Visible = false;
            TagLabel.Visible = false;
            label4.Visible = false;

            ThisClock = new AlarmClock();
            MyRadio = new Radio();
            tt = new Timer();

            foreach (string s in MyRadio.Titles)
            {
                StationsList.Items.Add(s);
            }

            ShowTimeAndTags();
            AlarmTimeLabel.Text = ThisClock.GetAlarmTime();
        }
        private void ShowTimeAndTags()
        {
            Timer t = new Timer();
            t.Interval = 500;
            t.Tick += delegate (object sender, EventArgs e)
            {
                Time.Text = ThisClock.GetTime();
                AlarmTimeLabel.Text = ThisClock.GetAlarmTime();
                if (ModeSwitcher.Value > 2)
                {
                    MyRadio.GetTags(StationsList.SelectedIndex);
                    TagLabel.Text = MyRadio.Artist + "\n" + MyRadio.Song;
                }
                if (ThisClock.AlarmRaised)
                {
                    ThisClock.AlarmRaised = false;
                    AlarmNotification f2 = new AlarmNotification();
                    f2.StartPosition = FormStartPosition.CenterParent;
                    f2.ShowDialog(this);
                }
            };
            t.Start();
        }

        private void SetupTimeBtn_Click(object sender, EventArgs e)
        {
            HoursBtn.Visible = !HoursBtn.Visible;
            MinsBtn.Visible = !MinsBtn.Visible;
            SetupAlarmBtn.Visible = !SetupAlarmBtn.Visible;

            ButtonMode = 1;
        }

        private void SetupAlarmBtn_Click(object sender, EventArgs e)
        {
            HoursBtn.Visible = !HoursBtn.Visible;
            MinsBtn.Visible = !MinsBtn.Visible;
            SetupTimeBtn.Visible = !SetupTimeBtn.Visible;

            ButtonMode = 2;
        }

        private void HoursBtn_Click(object sender, EventArgs e)
        {
            if (ButtonMode == 1)
                ThisClock.CurrentTime = ThisClock.CurrentTime.AddHours(1);
            else
            {
                ThisClock.AlarmTime = ThisClock.AlarmTime.AddHours(1);
                AlarmTimeLabel.Text = ThisClock.GetAlarmTime();
            }
        }

        private void MinsBtn_Click(object sender, EventArgs e)
        {
            if (ButtonMode == 1)
                ThisClock.CurrentTime = ThisClock.CurrentTime.AddMinutes(1);
            else
            {
                ThisClock.AlarmTime = ThisClock.AlarmTime.AddMinutes(1);
                AlarmTimeLabel.Text = ThisClock.GetAlarmTime();
            }
        }

        private void ModeSwitcher_ValueChanged(object sender, EventArgs e)
        {
            switch (ModeSwitcher.Value)
            {
                case 1:
                    {
                        StationsList.Enabled = false;
                        VolumeSlider.Enabled = false;
                        TagLabel.Visible = false;
                        label4.Visible = false;
                        //
                        tt.Stop();
                        StationsList.ClearSelected();
                        MyRadio.Stop();
                        ThisClock.TurnOn();
                    }
                    break;
                case 2:
                    {
                        StationsList.Enabled = false;
                        VolumeSlider.Enabled = false;
                        TagLabel.Visible = false;
                        label4.Visible = false;
                        //
                        tt.Stop();
                        StationsList.ClearSelected();
                        MyRadio.Stop();
                        ThisClock.TurnOff();            
                    }
                    break;
                case 3:
                    {
                        StationsList.Enabled = true;
                        VolumeSlider.Enabled = true;
                        TagLabel.Visible = true;
                        label4.Visible = true;
                        //
                        ThisClock.TurnOff();
                        tt.Stop();
                        StationsList.SelectedIndex = 0;
                        MyRadio.ChangeVolume((float)VolumeSlider.Value / 100);
                        Task PlayRadio = new Task(() => MyRadio.Start(StationsList.SelectedIndex));
                        PlayRadio.Start();
                    }
                    break;
                case 4:
                    {
                        StationsList.Enabled = true;
                        VolumeSlider.Enabled = true;
                        TagLabel.Visible = true;
                        label4.Visible = true;
                        //
                        ThisClock.TurnOff();
                        StationsList.SelectedIndex = 0;
                        MyRadio.ChangeVolume((float)VolumeSlider.Value / 100);
                        Task PlayRadio = new Task(() => MyRadio.Start(StationsList.SelectedIndex));
                        PlayRadio.Start();

                        tt.Interval = 1000 * 60;
                        tt.Tick += delegate (object obj, EventArgs ev)
                          {
                              MyRadio.Stop();
                              ModeSwitcher.Value = 1;
                              tt.Stop();
                          };
                        tt.Start();
                    }
                    break;
            }
        }
        private void StationsList_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            Graphics g = e.Graphics;
            Brush brush = ((e.State & DrawItemState.Selected) == DrawItemState.Selected) ?
                          Brushes.Red : new SolidBrush(e.BackColor);
            g.FillRectangle(brush, e.Bounds);
            e.Graphics.DrawString(StationsList.Items[e.Index].ToString(), e.Font,
                     new SolidBrush(e.ForeColor), e.Bounds, StringFormat.GenericDefault);
            e.DrawFocusRectangle();
        }

        private void VolumeSlider_ValueChanged(object sender, EventArgs e)
        {
            MyRadio.ChangeVolume((float)VolumeSlider.Value / 100);
        }

        private void StationsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            MyRadio.Stop();
            Task PlayRadio = new Task(() => MyRadio.Start(StationsList.SelectedIndex));
            PlayRadio.Start();
        }

        private void MinimizeButton_MouseHover(object sender, EventArgs e)
        {
            MinimizeButton.BackColor = Color.Red;
        }

        private void MinimizeButton_MouseLeave(object sender, EventArgs e)
        {
            MinimizeButton.BackColor = Color.Transparent;
        }

        private void CloseButton_MouseHover(object sender, EventArgs e)
        {
            CloseButton.BackColor = Color.Red;
        }

        private void CloseButton_MouseLeave(object sender, EventArgs e)
        {
            CloseButton.BackColor = Color.Transparent;
        }

        private void RadioSTButton_Click(object sender, EventArgs e)
        {
            RadioSettings f2 = new RadioSettings();
            f2.StartPosition = FormStartPosition.CenterParent;
            f2.ShowDialog(this);
        }
    }
}
