﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace AlarmClock
{
    class AlarmClock : Clock
    {
        public DateTime AlarmTime { get; set; } //Зберігає час спрацьовування будильника
        public bool AlarmRaised { get; set; } //Зберігає інформацію про те, чи подається сигнал

        private SoundPlayer alarmPlayer;
        private Timer alarmTimer; //Внутрішній таймер будильника
        public AlarmClock() : base()
        {
            alarmTimer = new Timer();
            alarmTimer.Interval = 1000;
            alarmTimer.Tick += delegate (object sender, EventArgs e)
              {
                  if (this.CurrentTime.ToLongTimeString() == AlarmTime.ToLongTimeString())
                  {
                      alarmTimer.Stop();
                      RaiseAlarm();
                  }
              };
            AlarmTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 7, 0, 0);
            alarmPlayer = new SoundPlayer(Properties.Resources.Alarm_tone);
            AlarmRaised = false;
        }
        private void RaiseAlarm() //Починає подачу сигналу при досягненні часу спрацьовування
        {
            alarmTimer.Stop();
            alarmPlayer.PlayLooping();
            AlarmRaised = true;
        }
        public void TerminateAlarm() //Припиняє подачу сигналу
        {
            alarmPlayer.Stop();
            AlarmRaised = false;
        }
        public void ResumeAlarm() //При ігноруванні сигналу повторює його через деякий час
        {
            alarmPlayer.Stop();
            AlarmTime = AlarmTime.AddMinutes(2);
            AlarmRaised = false;
            alarmTimer.Start();
        }
        public void TurnOn() //Вмикає будильник
        {
            alarmTimer.Start();
        }
        public void TurnOff() //Вимикає будильник
        {
            alarmTimer.Stop();
        }
        public string GetAlarmTime() //Представлення часу спрацьовування у потрібному вигляді
        {
            return (AlarmTime.Hour < 10 ? "0" + AlarmTime.Hour.ToString() : AlarmTime.Hour.ToString()) + ":" +
                (AlarmTime.Minute < 10 ? "0" + AlarmTime.Minute.ToString() : AlarmTime.Minute.ToString());
        }
    }
}
