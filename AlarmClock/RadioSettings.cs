﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AlarmClock
{
    public partial class RadioSettings : Form
    {
        public RadioSettings()
        {
            InitializeComponent();
        }

        private void RadioSettings_Load(object sender, EventArgs e)
        {
            StationsList.DrawMode = DrawMode.OwnerDrawFixed;
            StationsList.DrawItem += new DrawItemEventHandler(StationsList_DrawItem);

            ((Application)this.Owner).ModeSwitcher.Value = 2;

            LoadStations();

            StationsList.SelectedIndex = 0;

            ErrorMessage.Visible = false;
        }

        private void LoadStations()
        {
            StationsList.Items.Clear();
            ((Application)this.Owner).StationsList.Items.Clear();
            foreach (string s in ((Application)this.Owner).MyRadio.Titles)
            {
                StationsList.Items.Add(s);
                ((Application)this.Owner).StationsList.Items.Add(s);
            }
        }

        private void StationsList_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            Graphics g = e.Graphics;
            Brush brush = ((e.State & DrawItemState.Selected) == DrawItemState.Selected) ?
                          Brushes.Red : new SolidBrush(e.BackColor);
            g.FillRectangle(brush, e.Bounds);
            e.Graphics.DrawString(StationsList.Items[e.Index].ToString(), e.Font,
                     new SolidBrush(e.ForeColor), e.Bounds, StringFormat.GenericDefault);
            e.DrawFocusRectangle();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void CloseButton_MouseHover(object sender, EventArgs e)
        {
            CloseButton.BackColor = Color.Red;
        }

        private void CloseButton_MouseLeave(object sender, EventArgs e)
        {
            CloseButton.BackColor = Color.Transparent;
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            if (((Application)this.Owner).MyRadio.DeleteStation(StationsList.SelectedIndex))
            {
                int index = StationsList.SelectedIndex;

                LoadStations();

                if (StationsList.Items.Count - 1 < index)
                    StationsList.SelectedIndex = StationsList.Items.Count - 1;
                else
                    StationsList.SelectedIndex = index;

                ErrorMessage.Visible = false;
            }
            else
            {
                ErrorMessage.Visible = true;
                ErrorMessage.Text = "Only 1 Station Left";
            }
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            if (((Application)this.Owner).MyRadio.AddStation(NameBox.Text, URLBox.Text))
            {
                LoadStations();
                ErrorMessage.Visible = false;

                NameBox.Text = string.Empty;
                URLBox.Text = string.Empty;
            }
            else
            {
                ErrorMessage.Visible = true;
                ErrorMessage.Text = "Wrong Name or URL";
            }
        }
    }
}
