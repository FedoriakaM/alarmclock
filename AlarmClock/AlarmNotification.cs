﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlarmClock
{
    public partial class AlarmNotification : Form
    {
        public AlarmNotification()
        {
            InitializeComponent();
        }

        private Timer t;

        private void AlarmNotification_Load(object sender, EventArgs e)
        {
            t = new Timer();
            t.Interval = 60 * 1000;
            t.Tick += delegate (object obj, EventArgs ev)
             {
                 ((Application)this.Owner).ThisClock.ResumeAlarm();
                 t.Stop();
                 this.Dispose();
             };
            t.Start();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            t.Stop();
            ((Application)this.Owner).ThisClock.TerminateAlarm();
            ((Application)this.Owner).ModeSwitcher.Value = 2;
            this.Dispose();
        }
    }
}
