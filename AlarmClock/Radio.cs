﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Tags;
using System.Windows.Forms;
using System.IO;

namespace AlarmClock
{
    class Radio
    {
        public List<string> Stations { get; set; } //Список, що зберігає посилання на потоки для кожної радіостанції
        public List<string> Titles { get; set; } //Список, що зберігає назву для кожної радіостанції

        private bool playing; //Вказує на те, чи відбувається зараз відтворення звуку
        private int stream;
        private float volume; //Поточна гучність

        public string Artist { get; set; } //При відтворенні звуку за наявності інформації про виконавця зберігає її
        public string Song { get; set; } //При відтворенні звуку за наявності інформації про композицію зберігає її

        public Radio()
        {
            BassNet.Registration("fedoryaka.mg@gmail.com", "2X16231522152222");

            ReadStations();
            playing = false;
            stream = 0;
            
            Bass.BASS_Free();
        }

        private void ReadStations() //Зчитування списку станцій
        {
            Stations = new List<string>();
            Titles = new List<string>();

            System.IO.StreamReader file = new System.IO.StreamReader("stations.txt");
            while (!file.EndOfStream)
            {
                Titles.Add(file.ReadLine());
                Stations.Add(file.ReadLine());
                file.ReadLine();
            }
            file.Close();
        }

        public void Start(int number) //Починає відтворення ефіру радіостанції з заданим номером
        {
            playing = !playing;

            if (playing)
            {
                if (Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero))
                {
                    IntPtr ptr = new IntPtr(0);
                    stream = Bass.BASS_StreamCreateURL(Stations[number], 0, BASSFlag.BASS_DEFAULT, null, ptr);

                    Bass.BASS_ChannelPlay(stream, false);
                    Bass.BASS_ChannelSetAttribute(stream, BASSAttribute.BASS_ATTRIB_VOL, volume);
                }
            }
        }

        public void GetTags(int URL) //Отримує інформацію про композицію та виконавця
        {
            TAG_INFO tagInfo = new TAG_INFO(Stations[URL]);

            if (BassTags.BASS_TAG_GetFromURL(stream, tagInfo))
            {
                if (tagInfo.artist != "" && !tagInfo.artist.Contains('?') && tagInfo.artist!=Titles[URL] &&
                    tagInfo.title != "" && !tagInfo.title.Contains('?') && tagInfo.title != Titles[URL])
                {
                    Artist = tagInfo.artist;
                    Song = tagInfo.title;
                }
                else
                {
                    Artist = "No Tags";
                    Song = "";
                }
            }
            else
            {
                Artist = "No Tags";
                Song = "";
            }
        }

        public void Stop() //Припиняє відтворення ефіру радіостанції
        {
            playing = !playing;

            if (!playing)
            {
                Bass.BASS_StreamFree(stream);
                Bass.BASS_Free();
                stream = 0;
            }
        }

        public void ChangeVolume(float volume) //Змінює гучність
        {
            Bass.BASS_ChannelSetAttribute(stream, BASSAttribute.BASS_ATTRIB_VOL, volume);
            this.volume = volume;
        }

        private void RewriteFile() //Перезаписує файл з доступними радіостанціями
        {
            File.Delete("stations.txt");
            System.IO.StreamWriter file = new System.IO.StreamWriter("stations.txt");
            for (int i = 0; i < Titles.Count; i++)
            {
                file.WriteLine(Titles[i]);
                file.WriteLine(Stations[i]);
                file.WriteLine();
            }
            file.Close();
        }

        public bool AddStation(string name, string url) //Додає задану радіостанцію
        {
            Uri uriResult;
            bool IsValid = Uri.TryCreate(url, UriKind.Absolute, out uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            if (!(name == "") && Stations.Count < 9 && IsValid)
            {
                Titles.Add(name);
                Stations.Add(url);

                RewriteFile();
                return true;
            }
            else return false;
        }

        public bool DeleteStation(int index) //Видаляє задану радіостанцію
        {
            if (Stations.Count > 1)
            {
                Titles.RemoveAt(index);
                Stations.RemoveAt(index);

                RewriteFile();
                return true;
            }
            else return false;
        }
    }
}
